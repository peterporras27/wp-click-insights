<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://iampeter.info
 * @since      1.0.0
 *
 * @package    Sg_Click_Insights
 * @subpackage Sg_Click_Insights/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Sg_Click_Insights
 * @subpackage Sg_Click_Insights/public
 * @author     Peter Porras <peter@seoguarantee.com>
 */
class Sg_Click_Insights_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sg_Click_Insights_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sg_Click_Insights_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/sg-click-insights-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sg_Click_Insights_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sg_Click_Insights_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/sg-click-insights-public.js', array( 'jquery' ), $this->version, true );

		wp_localize_script(
			$this->plugin_name,
			'sgclicks',
			array(
				'ajax' => admin_url( 'admin-ajax.php' ),
				'data' => get_option('sg_click_insights_option_name')
			)
		);

	}

	/**
	 * Register ajax clicks in front end.
	 *
	 * @since    1.0.0
	 */
	public function sg_save_listened_clicks() 
	{

		global $wpdb;

	    $table_name = $wpdb->prefix . "sg_clicks"; 
	    $timestamp = '';
	    $type = '';
	    $destination = '';
	    $origin = '';
	    $target = '';
	    $attributes = '';

	    $types = ['class','id','tagname','attribute'];

	    $isvalid = true;

	    if (isset($_POST['timestamp'])) {
	    	$timestamp = strip_tags($_POST['timestamp']);
	    	if (empty($timestamp)) {
	    		$isvalid = false;
	    	}
	    }

	    if (isset($_POST['type'])) {
	    	$type = strip_tags($_POST['type']);
	    	if (!in_array($type, $types)) {
	    		$isvalid = false;
	    	}
	    }

	    if (isset($_POST['destination'])) {
	    	$destination = strip_tags($_POST['destination']);
	    }

	    if (isset($_POST['origin'])) {
	    	$origin = strip_tags($_POST['origin']);
	    	if (empty($origin)) {
	    		$isvalid = false;
	    	}
	    }

	    if (isset($_POST['target'])) {
	    	$target = strip_tags($_POST['target']);
	    }

	    if (isset($_POST['attributes'])) {
	    	$attributes = json_encode($_POST['attributes']);
	    }

	    $return = array();

	    if ($isvalid) 
	    {
			$wpdb->insert( 
				$table_name,
				array(
					'type' => $type,
					'destination' => $destination,
					'origin' => $origin,
					'target' => $target,
					'attributes' => $attributes,
					'ip_address' => $this->getUserIP()
				),
				array('%s','%s','%s','%s','%s','%s')
			);

			$return['last_error'] = $wpdb->last_error;
		}

		echo json_encode($return);

		exit();
	}

	/**
	 * Get visitor IP address
	 *
	 * @since    1.0.0
	 */
	public function getUserIP()
	{
	    // Get real visitor IP behind CloudFlare network
	    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
	    }
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];

	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }

	    return $ip;
	}

}
