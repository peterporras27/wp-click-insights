<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://iampeter.info
 * @since      1.0.0
 *
 * @package    Sg_Click_Insights
 * @subpackage Sg_Click_Insights/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Sg_Click_Insights
 * @subpackage Sg_Click_Insights/admin
 * @author     Peter Porras <peter@seoguarantee.com>
 */
class Sg_Click_Insights_Admin {

	private $sg_click_insights_options;
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sg_Click_Insights_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sg_Click_Insights_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name.'-chart-js', '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.css', array(), '2.9.4', 'all' );
		wp_enqueue_style( $this->plugin_name.'-grid', plugin_dir_url( __FILE__ ) . 'css/custom-grid.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/sg-click-insights-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sg_Click_Insights_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sg_Click_Insights_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name.'-chart-js', '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js', array( 'jquery' ), '2.9.4', false );
		wp_enqueue_script( $this->plugin_name.'-utils-js', plugin_dir_url( __FILE__ ) . 'js/sg-chart.js', array( 'jquery', $this->plugin_name.'-chart-js' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/sg-click-insights-admin.js', array( 'jquery', $this->plugin_name.'-chart-js' ), $this->version, false );

		wp_localize_script(
			$this->plugin_name,
			'sgclicks',
			array(
				'ajax' => admin_url( 'admin-ajax.php' ),
				'data' => get_option('sg_click_insights_option_name')
			)
		);
	}

	/**
	 * Pull data records in database.
	 *
	 * @since    1.0.0
	 */
	public function sg_pull_data_saved()
	{
	    $return = array(
	    	'sent' => $_GET,
	    	'total' => 0,
	    	'data' => ''
	    );

	   	if (isset($_GET['type'])) :

	   		$return['data'] = $this->sg_get_data_insights($_GET);
	    
	   	endif;

	    echo json_encode($return);

		exit();
	}

	public function sg_get_data_insights($arr)
	{
		global $wpdb;
	    $table_name = $wpdb->prefix . "sg_clicks";
	    $query = "SELECT * FROM $table_name";
	    $total_query = "SELECT COUNT(*) FROM $table_name";
		$insights = get_option( 'sg_click_insights_option_name' );
		if ($insights) { $insights = $insights['sg_click_data']; } else {return '';}

		$return = [];

		foreach ($insights as $k => $v) 
		{
			$type = $v['type'];
			$target = $v['target'];

			switch ($arr['type']) {
				case 'year':
					
					$from = preg_replace('/\D/', '', $arr['from']); // allow numbers only
					$to = preg_replace('/\D/', '', $arr['to']); // allow numbers only

					$return[] = array(
						'type' => $type,
						'target' => $target,
						'data' => $wpdb->get_results( $query." WHERE type = '$type' AND target = '$target' AND YEAR(timestamp) BETWEEN '$from' AND '$to'")
					);

					break;
				case 'week':
					
					$month = date_parse($arr['month']);
					$month = $month['month'];
					$year = preg_replace('/\D/', '', $arr['year']); // allow numbers only
					
					$return[] = array(
						'type' => $type,
						'target' => $target,
						'data' => $wpdb->get_results( $query." WHERE type = '$type' AND target = '$target' AND MONTH(timestamp) = '$month' AND YEAR(timestamp) = '$year'")
					);

					break;
				case 'day':
					
					$month = date_parse($arr['month']);
					$day = preg_replace('/\D/', '', $arr['day']); // allow numbers only
					$year = preg_replace('/\D/', '', $arr['year']); // allow numbers only
					$date = $year.'-'.$month['month'].'-'.$day;

					$return[] = array(
						'type' => $type,
						'target' => $target,
						'data' => $wpdb->get_results( $query." WHERE type = '$type' AND target = '$target' AND DATE(timestamp) = '$date'")
					);

					break;
				default: // month is default

					$year = preg_replace('/\D/', '', $arr['year']); // allow numbers only
					
					$return[] = array(
						'type' => $type,
						'target' => $target,
						'data' => $wpdb->get_results( $query." WHERE type = '$type' AND target = '$target' AND YEAR(timestamp) = '$year'")
					);

					break;
			}
		}

		return $return;
	}

	public function sg_click_insights_add_plugin_page() 
	{

		/* 
		 * Retrieve this value with:
		 * $sg_click_insights_options = get_option( 'sg_click_insights_option_name' ); // Array of All Options
		 * $click_type_0 = $sg_click_insights_options['click_type_0']; // Click Type
		 * $data = $sg_click_insights_options['data']; // Target Value
		 */

		add_menu_page(
			'SG Click Insights', // page_title
			'SG Click', // menu_title
			'manage_options', // capability
			'sg-click-insights', // menu_slug
			array( $this, 'sg_click_insights_create_admin_page' ), // function
			'dashicons-admin-links', // icon_url
			76 // position
		);
	}

	public function sg_click_insights_create_admin_page() 
	{
		$this->sg_click_insights_options = get_option( 'sg_click_insights_option_name' ); 
		?>
		<style>.table-responsive th{background: #23282d;color:#fff;text-align: left;}</style>
		<div class="wrap">
			<h2>SG Click Insights</h2>
			<p>Track specific clicks in all your WordPress links or elements.</p>
			<?php settings_errors(); ?>
			<form id="sg-click-form" method="post" action="options.php">
				<?php
					settings_fields( 'sg_click_insights_option_group' );
					do_settings_sections( 'sg-click-insights-admin' );
				?>
			</form>
		</div>
		<?php 
	}

	public function sg_click_insights_page_init() 
	{
		register_setting(
			'sg_click_insights_option_group', // option_group
			'sg_click_insights_option_name', // option_name
			array( $this, 'sg_click_insights_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'sg_click_insights_setting_section', // id
			'Target Elements', // title
			array( $this, 'sg_click_data_values_callback' ), // callback
			'sg-click-insights-admin' // page
		);
	}

	public function sg_click_insights_sanitize($input) 
	{
		$sanitary_values = array();

		if ( isset( $input['sg_click_data'] ) ) {
			foreach ($input['sg_click_data'] as $data => $val) {
				$sanitary_values['sg_click_data'][] = array(
					'type' => strip_tags($val['type']),
					'target' => strip_tags($val['target'])
				);	
			}
		}

		return $sanitary_values;
	}

	public function sg_click_data_values_callback() 
	{
		$options = get_option( 'sg_click_insights_option_name' ); 
		$data = isset( $options['sg_click_data'] ) ? $options['sg_click_data'] : [];
		$count = 0;
		?>
		<script type="text/javascript">var targetDatas = <?php echo ($options['sg_click_data']) ?json_encode($options['sg_click_data']):'[]'; ?>;</script>
		<div class="card" style="margin:0;padding:15px;width: 100%;max-width: 100%;">
			<span><b>Target element using</b></span>
			<select id="sg-type">
				<option value="class">Class Name</option>
				<option value="id">ID</option>
				<option value="tagname">HTML Tag</option>
				<option value="attribute">Attribute Name</option>
			</select>
			<input type="text" id="sg-target" placeholder="Class Name">
			<a href="#" id="sg-add-data" class="button button-primary">Add</a>
			<hr>
			<div class="targets table-responsive">
				<table class="table table-hover table-bordered">
					<thead>
						<tr>
							<th>Target Type</th>
							<th>Name</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
						<?php if($data): ?>
							<?php foreach($data as $a => $b): ?>
								<tr>
									<td>
										<?=$b['type'];?>
										<input type="hidden" name="sg_click_insights_option_name[sg_click_data][<?=$count;?>][type]" value="<?=$b['type'];?>">
									</td>
									<td>
										<?=$b['target'];?>
										<input type="hidden" name="sg_click_insights_option_name[sg_click_data][<?=$count;?>][target]" value="<?=$b['target'];?>">
									</td>
									<td>
										<a href="#" onclick="removeSG(this,event)">Delete</a>
									</td>
								</tr>
								<?php $count++; ?>
							<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
			<?php submit_button(); ?>
		</div>

		<h2>Statistics</h2>
		<?php $dates = $this->sg_get_dates_dropdown(); ?>

		<div class="card" style="margin:0;width: 100%;max-width: 100%;">
			<div class="chart-filter">
				<span>Display By:</span> 
				<select id="sg-chart-filter">
					<option value="year">Year</option>
					<option value="month" selected>Month</option>
					<option value="week">Week</option>
					<option value="day">Day</option>
				</select>
				&nbsp;&nbsp;

				<div class="display-year" style="display: none;">
					<span>From:</span>
					<select id="year-filter-from">
						<?php echo implode('', $dates['year']['from']); ?>
					</select>
					<span>&nbsp;To:</span> 
					<select id="year-filter-to">
						<?php echo implode('', $dates['year']['to']); ?>
					</select>
				</div>

				<div class="display-month" style="display: inline-block;">
					<span>Year:</span>
					<select id="month-filter-year">
						<?php echo implode('', $dates['year']['merged']); ?>
					</select>
				</div>

				<div class="display-week" style="display: none;">
					<span>&nbsp;Month:</span>
					<select id="week-filter-month">
						<?php echo implode('', $dates['month']['to']); ?>
					</select>
					<span>Year:</span>
					<select id="week-filter-year">
						<?php echo implode('', $dates['year']['merged']); ?>
					</select>
				</div>

				<div class="display-day" style="display: none;">
					<span>&nbsp;Month:</span>
					<select id="day-filter-month">
						<?php echo implode('', $dates['month']['to']); ?>
					</select>
					<span>&nbsp;Day:</span>
					<select id="day-filter-day">
						<?php echo implode('', $dates['day']); ?>
					</select>
					<span>Year:</span>
					<select id="day-filter-year">
						<?php echo implode('', $dates['year']['merged']); ?>
					</select>
				</div>
				
				<a id="sg-filter-data" href="#" class="button button-primary">Filter</a>
			</div>
			<hr>
			<canvas id="click-chart" style="width:100%;height: 400px;max-height: 410px;"></canvas>
		</div>

		<h2>Click Records</h2>
		<div class="card" style="margin:0;width: 100%;max-width: 100%;">
			<?php $this->sg_pull_data_list(); ?>
		</div>

		<?php
	}

	public function sg_get_dates_dropdown()
	{
		$return = array();
		$years = [];

		$fromYearStart=date('Y');
		$fromEndYear=$fromYearStart-10;
		$yearFromArray = range($fromYearStart,$fromEndYear);
		foreach ($yearFromArray as $fyear) {
	        $selected = ($fyear == $fromYearStart) ? 'selected' : '';
	        $return['year']['from'][] = '<option '.$selected.' value="'.$fyear.'">'.$fyear.'</option>';
	        $years[] = $fyear;
	    }

	    $toYearStart=date('Y')+1;
		$toEndYear=$toYearStart+10;
		$yearToArray = range($toYearStart,$toEndYear);
		foreach ($yearToArray as $tyear) {
	        $return['year']['to'][] = '<option value="'.$tyear.'">'.$tyear.'</option>';
	        $years[] = $tyear;
	    }

	    asort($years);
	    foreach ($years as $yr) {
	    	$selected = ($yr == date('Y')) ? 'selected' : '';
	    	$return['year']['merged'][] = '<option '.$selected.' value="'.$yr.'">'.$yr.'</option>';
	    }
	    

	    $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	    foreach ($months as $month) {
	    	$m = strtolower($month);
	    	$f = strtolower(date('F'));
	    	$selected = ($m == $f) ? 'selected' : '';
	    	$return['month']['from'][] = '<option '.$selected.' value="'.$m.'">'.$month.'</option>';
	    	$return['month']['to'][] = '<option '.$selected.' value="'.$m.'">'.$month.'</option>';
	    }

	    $days = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
	    $number = cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y'));
	    for ($i = 1; $i <= $number; $i++) {
	    	$selected = (date('j') == $i) ? 'selected' : '';
	    	$return['day'][] = '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
	    }

		return $return;
	}

	public function sg_pull_data_list()
	{
	    global $wpdb;
	    $table_name = $wpdb->prefix . "sg_clicks"; 
	    $query = "SELECT * FROM $table_name";

	    $total_query = "SELECT COUNT(1) FROM $table_name";
	    $total = $wpdb->get_var( $total_query );
	    $items_per_page = 10;
	    $page = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
	    $offset = ( $page * $items_per_page ) - $items_per_page;
	    $results = $wpdb->get_results( $query . " ORDER BY timestamp LIMIT ${offset}, ${items_per_page}" );

	    if(!empty($results)) : ?>
		<div class="table-responsive">
		    <table class='table table-hover table-bordered'>
		    	<thead>
		    		<tr>
		    			<th>Type</th>
		    			<th>Target</th>
		    			<th>Origin</th>
		    			<th>Destination</th>
		    			<th>IP Address</th>
		    			<th>Date</th>
		    		</tr>
		    	</thead>
			    <tbody>      
			    <?php foreach($results as $row): ?>
				    <tr>
				    	<td><?=$row->type;?></td>
		    			<td><?=$row->target;?></td>
		    			<td><?=$row->origin;?></td>
		    			<td><?=$row->destination;?></td>
		    			<td><?=$row->ip_address;?></td>
		    			<td><?=$row->timestamp;?></td>
				    </tr>
			    <?php endforeach; ?>
			    </tbody>
		    </table> 
		</div>
		<?php 
		endif;

	    echo paginate_links( array(
	        'base' => add_query_arg( 'cpage', '%#%' ),
	        'format' => '',
	        'prev_text' => __('&laquo;'),
	        'next_text' => __('&raquo;'),
	        'total' => ceil($total / $items_per_page),
	        'current' => $page
	    ));
	}

}
