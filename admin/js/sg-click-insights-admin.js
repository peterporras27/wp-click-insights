jQuery(document).ready(function() {
	var $=jQuery;

	$('#sg-add-data').click(function(event) {
		event.preventDefault();

		var c = $('.targets tr').length;
		var type = $('#sg-type').val();
		var target = $('#sg-target').val();

		var exist = containsObject({type:type,target:target});
		if (exist) {

			$('#sg-target').val('');

		}else{

			if (target!='') {
				var input = '<tr>';
				input += '<td>'+type+'<input type="hidden" name="sg_click_insights_option_name[sg_click_data]['+c+'][type]" value="'+type+'"></td>';
				input += '<td>'+target+'<input type="hidden" name="sg_click_insights_option_name[sg_click_data]['+c+'][target]" value="'+target+'"></td>';
				input += '<td><a href="#" onclick="removeSG(this,event)">Delete</a></td>';
				input += '</tr>';
				$('#sg-click-form .targets tbody').append(input);
			}
		}
	});

	$('#sg-type').change(function() {
		$('#sg-target').attr('placeholder',$(this).find('option:selected').text());
	});

	$('#sg-chart-filter').change(function(){
		
		var items = ['year','month','week','day'];

		for (var i = items.length - 1; i >= 0; i--) 
		{
			if ($(this).val()==items[i]) {
				$('.display-'+items[i]).attr('style','display:inline-block;');
			} else {
				$('.display-'+items[i]).attr('style','display:none;');
			}
		}
	});

	$('#chart-year-filter-from, #chart-year-filter-to').keyup(function(event) {
		$(this).val($(this).val().replace(/[^\d]/g, ''));
	});

	$('#sg-filter-data').click(function(e){
		e.preventDefault();
		pullSgChartData();
	});

	pullSgChartData();
});


function containsObject(obj) {
    var x;

    for (var i = targetDatas.length - 1; i >= 0; i--) {
    	if (targetDatas[i].type==obj.type&&targetDatas[i].target==obj.target) {
    		return true;
    	}
    }

    if (obj.target!='') {
    	targetDatas.push(obj);
    }

    return false;
}

function pullSgChartData()
{
	var $=jQuery;
	var type = $('#sg-chart-filter').val();
	var targetChart = document.getElementById('click-chart');
	var presets = window.chartColors;
	var utils = Sg.utils;

	var options = {
        scales: {
            yAxes: [{
                ticks: { beginAtZero: true},
                stacked: true
            }]
        },
        elements: {
			line: { tension: 0.000001 }
		},
    };

    var data = { 
    	labels: window.MONTHS, 
    	datasets: [] 
    };

	SgParams.type = type;

	switch(type){
		case 'year':
			SgParams.from = $('#year-filter-from').val();
			SgParams.to = $('#year-filter-to').val();
			data.labels = getYearLabels(SgParams.from,SgParams.to);
			break;
		case 'month':
			SgParams.year = $('#month-filter-year').val();
			data.labels = window.MONTHS;
			break;
		case 'week':
			SgParams.month = $('#week-filter-month').val();
			SgParams.year = $('#week-filter-year').val();
			data.labels = getWeeksLabel(SgParams.year, SgParams.month);
			break;
		case 'day':
			SgParams.month = $('#day-filter-month').val();
			SgParams.day = $('#day-filter-day').val();
			SgParams.year = $('#day-filter-year').val();
			data.labels = [
				jsUcfirst(SgParams.month)+' '+(parseFloat(SgParams.day)-1)+', '+SgParams.year,
				jsUcfirst(SgParams.month)+' '+SgParams.day+', '+SgParams.year,
				jsUcfirst(SgParams.month)+' '+(parseFloat(SgParams.day)+1)+', '+SgParams.year,
			];
			break;
	}

	$.ajax({
		url: sgclicks.ajax,
		type: 'GET',
		dataType: 'json',
		data: SgParams,
	}).fail(function(e) {
		console.log(e);
	}).always(function(res) {

		var l = res.data.length;
		
		if ( l > 0 ) {

			var datasets = [];
			var j=0;

			for (var i = 0; i < l; i++) {

				var color = '#8549ba';
				if (typeof window.COLORS[j] === 'undefined') {
					j=0; color = window.COLORS[j];
				} else {
					color = window.COLORS[j];
				}

				datasets.push({
					backgroundColor: utils.trans(color),
					borderColor: color,
					data: parseDataType( res.data[i].data, type ),
					borderWidth: 1,
					label: res.data[i].type+' - '+res.data[i].target
				});

				j++;
			}
		
			data.datasets = datasets;
			
			if (SgChart===null) {

				SgChart = new Chart(targetChart, {
					type: 'line',
					data: data,
					options: options
				});

			} else {
				
				SgChart.data = data;
				SgChart.update();
			}
			
		}
	});
}

function parseDataType(data, type)
{
	var ret = [];

	switch( type )
	{
		case 'week':

			var year = SgParams.year;
			var month = SgParams.month;

			var newMonths = [];
			for (var i = 0; i < window.MONTHS.length; i++) {
				newMonths.push(window.MONTHS[i].toLowerCase());
			}

			month = newMonths.indexOf(month)+1;

			var weeksCount = week(year, month);
			var day = new Date(year, month - 1, 1).getDay();
			var days = new Date(year, month, 0).getDate();

			var labels = [];
			var weeks = {};

			// create container for weeks and data
			for (var i = 1; i <= weeksCount; i++) { weeks[i] = [];}
			for (var j = 0; j < weeksCount; j++) {ret[j] = 0;}

			var w = 1;

			for (var k = 1; k <= days; k++) 
			{
				if (typeof weeks[w] !== 'undefined' ) { weeks[w].push(k); }
				if (day==6) { day = -1; w++; }
				day++;
			}

			for (var l = 0; l < data.length; l++) 
			{
				var g = new Date(data[l].timestamp);
				var result = getIndexOfK(weeks, g.getDate());

				ret[result[0]] = ret[result[0]]+1;
			}
		
		break;

		case 'day':

			// default values for months
			ret.push(0);
			ret.push(data.length);
			ret.push(0);

		break;

		case 'year':

			// default value for years

			var obj = {};
			var yr = Number(SgParams.from);

			obj[yr] = 0;

			for (var i = SgParams.from; i < SgParams.to; i++) {
				yr = parseFloat(yr)+1;
				obj[yr] = 0;
			}

			if (data.length>0) 
			{
				for (var i = 0; i < data.length; i++) 
				{
					var d = new Date(data[i].timestamp);
					var y = d.getFullYear();

					obj[y] = parseFloat(obj[y])+1;
				}
			}

			for ( var key in obj ) {
				if ( obj.hasOwnProperty( key ) ) {
					ret.push(obj[key]);
				}
			}

		break;
		case 'month':

			// default values for months
			for (var i = 0; i < 12; i++) { ret[i] = 0;}
			if (data.length>0) {
				for (var i = 0; i < data.length; i++) {

					var d = new Date(data[i].timestamp);
					ret[d.getMonth()] = ret[d.getMonth()]+1;
				}
			}

		break;
	}

	return ret;
}

function getIndexOfK(arr, k) 
{
	for ( var key in arr ) {
		if ( arr.hasOwnProperty( key ) ) {
			var index = arr[key].indexOf(k);
		    if (index > -1) {
		      	return [key, index];
		    }
		}
	}
}

function getWeeksLabel(year, month)
{
	var newMonths = [];
	for (var i = 0; i < window.MONTHS.length; i++) {
		newMonths.push(window.MONTHS[i].toLowerCase());
	}

	month = newMonths.indexOf(month)+1;

	var weeksCount = week(year, month);
	var day = new Date(year, month - 1, 1).getDay();
	var days = new Date(year, month, 0).getDate();

	var labels = [];
	var weeks = {};

	// create container for labels
	for (var i = 1; i <= weeksCount; i++) { weeks[i] = []; }

	var w = 1;
	
	for (var i = 1; i <= days; i++) 
	{
		if (typeof weeks[w] !== 'undefined' ) { weeks[w].push(i); }
		if (day==6) { day = -1; w++; }
		day++;
	}

	for ( var key in weeks ) {
		if ( weeks.hasOwnProperty( key ) ) {
			if (weeks[key].length) {
				labels.push(window.MONTHS[month-1]+' '+weeks[key][0]+' - '+weeks[key][weeks[key].length-1]+' '+year);
			}
		}
	}

	return labels;	
}

function week(year, monthNumber) {

    var firstOfMonth = new Date(year, monthNumber, 1);
    var lastOfMonth = new Date(year, monthNumber + 1, 0);

    var used = firstOfMonth.getDay() + lastOfMonth.getDate();

    return Math.ceil( used / 7);
}

function totalWeeks(year, month) {
    var firstOfMonth = new Date(year, month - 1, 1);
    var day = firstOfMonth.getDay() || 6;
    day = day === 1 ? 0 : day;
    if (day) { day-- }
    var diff = 7 - day;
    var lastOfMonth = new Date(year, month, 0);
    var lastDate = lastOfMonth.getDate();
    if (lastOfMonth.getDay() === 1) {
        diff--;
    }
    var result = Math.ceil((lastDate - diff) / 7);
    return result + 1;
}

function monthStart(year, month) {
	var day = new Date(year + "-" + month + "-01").getDay();
	return day;
}

function getYearLabels(from, to)
{
	var yrs = [];
	for (var i = from; i <= to; i++) { yrs.push(i);}
	return yrs;
}

function removeSG(btn,event)
{
	event.preventDefault();
	jQuery(btn).parent().parent().remove();
}

function jsUcfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}