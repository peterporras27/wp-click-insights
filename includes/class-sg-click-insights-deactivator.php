<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://iampeter.info
 * @since      1.0.0
 *
 * @package    Sg_Click_Insights
 * @subpackage Sg_Click_Insights/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Sg_Click_Insights
 * @subpackage Sg_Click_Insights/includes
 * @author     Peter Porras <peter@seoguarantee.com>
 */
class Sg_Click_Insights_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() 
	{
		
		// Drop tables if needed. but backup is necessary.
		// global $wpdb;
	    // $table_name = $wpdb->prefix . "sg_clicks"; 
    	// $wpdb->query( "DROP TABLE IF EXISTS $table_name" ); // drop table
	    // delete_option("sg_click_insights_option_name"); // delete options
	}

}
