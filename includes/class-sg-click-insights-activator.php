<?php

/**
 * Fired during plugin activation
 *
 * @link       https://iampeter.info
 * @since      1.0.0
 *
 * @package    Sg_Click_Insights
 * @subpackage Sg_Click_Insights/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Sg_Click_Insights
 * @subpackage Sg_Click_Insights/includes
 * @author     Peter Porras <peter@seoguarantee.com>
 */
class Sg_Click_Insights_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() 
	{
		global $wpdb;

	    $table_name = $wpdb->prefix . "sg_clicks"; 
	    $charset_collate = $wpdb->get_charset_collate();

	    #Check to see if the table exists already, if not, then create it
	    if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) 
	    {

			$sql = "CREATE TABLE $table_name (
			  id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
			  timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  type varchar(100) NOT NULL DEFAULT '',
			  origin varchar(255) NOT NULL DEFAULT '',
			  destination varchar(255) NOT NULL DEFAULT '',
			  target varchar(255) NOT NULL DEFAULT '',
			  attributes longtext NOT NULL,
			  ip_address varchar(100) NOT NULL DEFAULT '',
			  PRIMARY KEY  (id)
			) $charset_collate;";

			self::create_table($sql);
	    }
	}

	public function create_table($sql)
	{
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
}
